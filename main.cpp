#include "fem_base.hpp"
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <numeric>
// #include <mars.hpp>
#include <err.h>

#ifdef WITH_PAR_MOONOLITH
// #include <mpi.h>
#endif // WITH_PAR_MOONOLITH

#ifdef WITH_MPI
#ifdef WITH_KOKKOS
#include "mars_advection.hpp"
#include "mars_poisson.hpp"
#endif // WITH_KOKKOS
#endif // WITH_MPI

#include <chrono>

using namespace std::chrono;

void run_benchmarks(int level, int refine_level) {
  using namespace mars;

  std::cout << "Generation level:" << level << std::endl;
  std::cout << "Refinement level:" << refine_level << std::endl;

#ifdef WITH_KOKKOS

  /*	ParallelMesh2 pMesh2;
  //generate_cube(pMesh2, level +6, level + 20 , 0);
  generate_cube(pMesh2, level, level, 0);
  ParallelLeppBenchmark<ParallelMesh2> b;
  b.run(refine_level, pMesh2, "pb");*/

  /*Mesh2 sMesh;
  convert_parallel_mesh_to_serial(sMesh, pMesh2);

  PreLeppBenchmark<Mesh2> b2;
  b2.run(refine_level, sMesh, "b2");*/
  // test_mars_mesh_generation_kokkos_3D(level,refine_level, level);
  // test_mars_nonsimplex_mesh_generation_kokkos_2D(level, refine_level);

  // test_mars_nonsimplex_mesh_generation_kokkos_3D(level, refine_level,
  // refine_level);

  /* ParallelQuad4Mesh nsm;
  generate_cube(nsm, level, refine_level, 0); */
  ParallelMesh3 pMesh3;
  generate_cube(pMesh3, level, level, level);

  ParallelLeppBenchmark<ParallelMesh3> b;
  b.run(refine_level,pMesh3, "pb");

  /*	Mesh3 sMesh3;
  convert_parallel_mesh_to_serial(sMesh3, pMesh3);

  PreLeppBenchmark<Mesh3> b3;
  b3.run(refine_level, sMesh3, "b3");*/
#endif
}

int main(int argc, char *argv[]) {
  using namespace mars;
  using namespace fem;

#ifdef WITH_PAR_MOONOLITH
  MPI_Init(&argc, &argv);
#endif

  // test_incomplete_bad_4D();
  // read_file();
  // write_file();

  int level = 1;
  int refine_level = 1;
  std::string filename = "../data/write/tetrakis.MFEM";
  if (argc > 1) {
    char *end_ptr = argv[1];
    level = strtol(argv[1], &end_ptr, 10);
    if (*end_ptr != '\0' || end_ptr == argv[1])
      warnx("'%s' could not be (completely) converted to long", argv[1]);

    if (argc == 3) {
      char *end_ptr = argv[2];
      refine_level = strtol(argv[2], &end_ptr, 10);
      if (*end_ptr != '\0' || end_ptr == argv[1])
        warnx("'%s' could not be (completely) converted to long", argv[1]);
    }

  } else
    std::cout
        << "No level of refinement was specified. Setting the default to 1!"
        << std::endl;

    /*if (argc > 2) {
            filename = argv[2];
    } else
            std::cout
                            << "No file name was specified. Setting the default
    to tetrakis!"
                            << std::endl;*/

    // run_tests(level,filename);

    // test_uniform_bisection_2D(level,filename);
    // test_read_write_3D(filename);

    /*test_mars_mesh_generation_3D(100,100,100);
    test_mars_mesh_generation_3D(78,100,80);*/

    // test_mars_mesh_generation_3D(150,150,150);
    // test_mars_mesh_generation_3D(200,200,200);

    // equivalent 3D generation using refinement and libmesh like mesh
    // generation technique.

    /*test_uniform_bisection_3D(3, test_mars_mesh_generation_3D(1,1,1));*/
    // parallel with kokkos.
#ifdef WITH_KOKKOS
  Kokkos::initialize(argc, argv);
  {

#ifdef USE_CUDA
    cudaDeviceSetLimit(cudaLimitStackSize, 32768);
// set stack to 32KB only for cuda since it is not yet supported in kokkos.
#endif

    run_benchmarks(level, refine_level);

    // test_mars_nonsimplex_mesh_generation_kokkos_2D(level, level);
    // test_mars_nonsimplex_mesh_generation_kokkos_3D(level, level, level);
    /* test_mars_distributed_nonsimplex_mesh_generation_kokkos_2D(argc, argv,
     * level); */

    /* test_mars_distributed_nonsimplex_mesh_generation_kokkos_3D(argc, argv,
     * level); */

    // test_mpi_context(argc, argv);
    // test_mars_mesh_generation_kokkos_2D(2,4);

    // test_mars_mesh_generation_kokkos_2D(level + 4,level);
    // test_mars_mesh_generation_kokkos_3D(level,level,level);
    // test_mars_mesh_generation_kokkos_1D(level);

    // advection(argc, argv, level);
    // poisson(argc, argv, level);
  }

  Kokkos::finalize();

#endif

#ifdef WITH_PAR_MOONOLITH
  run_mars_moonolith_test();
#endif // WITH_PAR_MOONOLITH

#ifdef WITH_PAR_MOONOLITH
  return MPI_Finalize();
#else
  return 0;
#endif
}
