cmake_minimum_required(VERSION 2.8.12)

project(fem)

option(TRY_WITH_MOONOLITH "Use -DTRY_WITH_MOONOLITH=ON for enabling mesh transfer functions." OFF)
option(TRY_WITH_KOKKOS "Use -DTRY_WITH_KOKKOS=ON for enabling mesh transfer functions." OFF)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING
      "Choose the type of build, options are: Debug Release
RelWithDebInfo MinSizeRel."
      FORCE)

  	message(STATUS "[Status] CMAKE_BUILD_TYPE=Release")
endif(NOT CMAKE_BUILD_TYPE)

#################################################################################
#################################################################################
#################################################################################


function(find_project_files rootPath dirPaths headers sources)
	SET(verbose TRUE)

	SET(theaders ${${headers}})
	SET(tsources ${${sources}})

	SET(ROOT_PATH ${${rootPath}})
	SET(DIR_PATHS ${${dirPaths}})

	FOREACH(INCLUDE_PATH ${DIR_PATHS})
		INCLUDE_DIRECTORIES(${ROOT_PATH}/${INCLUDE_PATH})

		file(GLOB TEMP_HPPSRC "${ROOT_PATH}/${INCLUDE_PATH}/*.cpp" )
		file(GLOB TEMP_SRC "${ROOT_PATH}/${INCLUDE_PATH}/*.c" )
		file(GLOB TEMP_HPPDR "${ROOT_PATH}/${INCLUDE_PATH}/*.hpp" )
		file(GLOB TEMP_HDR "${ROOT_PATH}/${INCLUDE_PATH}/*.h" )

		SOURCE_GROUP(${INCLUDE_PATH} FILES ${TEMP_HPPDR}; ${TEMP_HDR}; ${TEMP_HPPSRC}; ${TEMP_SRC}; ${TEMP_UI})

		SET(tsources ${tsources}; ${TEMP_SRC}; ${TEMP_HPPSRC})
		SET(theaders ${theaders}; ${TEMP_HDR}; ${TEMP_HPPDR})
	ENDFOREACH(INCLUDE_PATH)

	SET(${headers} ${theaders} PARENT_SCOPE)
	SET(${sources} ${tsources} PARENT_SCOPE)
endfunction()

#################################################################################
#################################################################################
#################################################################################

find_package(MPIExtended)

if(MPI_FOUND)
# 	set(CMAKE_CXX_COMPILER ${MPI_CXX_COMPILER})
# 	set(CMAKE_CXX_COMPILER_DEBUG ${MPI_CXX_COMPILER})
set(WITH_MPI ON)
endif()

find_package(MARS)

option(USE_CUDA "Build fem with cuda support" OFF)

if(TRY_WITH_KOKKOS)
	if (NOT TRILINOS_DIR)
	  message(STATUS "Setting TRILINOS_DIR to $ENV{TRILINOS_DIR}")
	  set(TRILINOS_DIR $ENV{TRILINOS_DIR} CACHE PATH "Directory where Kokkos is installed")
	endif()

	if (NOT KOKKOS_DIR)
	  message(STATUS "Setting KOKKOS_DIR to $ENV{KOKKOS_DIR}")
	  set(KOKKOS_DIR $ENV{KOKKOS_DIR} CACHE PATH "Directory where Kokkos is installed")
	endif()

	#FIND_PACKAGE(Trilinos PATHS ${TRILINOS_DIR}/lib/cmake/Trilinos QUIET)
	FIND_PACKAGE(Kokkos HINTS ${KOKKOS_DIR}/lib/CMake/Kokkos ${TRILINOS_DIR}/lib/cmake/Kokkos QUIET)

	IF(Kokkos_FOUND)
	   	MESSAGE("\nFound Kokkos!  Here are the details: ")
	   	MESSAGE("   Kokkos_CXX_COMPILER = ${Kokkos_CXX_COMPILER}")
	   	MESSAGE("   Kokkos_C_COMPILER = ${Kokkos_C_COMPILER}")
	   
	   	IF(Kokkos_CXX_COMPILER)
			SET(CMAKE_C_COMPILER ${Kokkos_C_COMPILER})
			SET(CMAKE_CXX_COMPILER ${Kokkos_CXX_COMPILER})
			MESSAGE("Setting CMAKE_CXX_COMPILER to Kokkos_CXX_COMPILER")
	   	ENDIF()
	   
	   	set(WITH_KOKKOS ON)
	   
        MESSAGE("WITH_KOKKOS: ${WITH_KOKKOS}")
		FIND_PACKAGE(OpenMP)

		IF(OPENMP_FOUND)
		 	SET (CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${OpenMP_Fortran_FLAGS}")
		 	SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
		 	SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
		 	set (CMAKE_CXX_LINK_FLAGS "${CMAKE_CXX_LINK_FLAGS} ${OpenMP_CXX_FLAGS}")
		ENDIF()
	   
	ELSE()
	  	MESSAGE(WARNING "Could not find Kokkos!")
	  	IF(USE_CUDA)
	    	MESSAGE(FATAL_ERROR "Could not use CUDA without Kokkos!")
	  	ENDIF()
	ENDIF()

endif()

list(APPEND FEM_HEADERS
	base/fem_base.hpp
	base/fem_test.hpp
	base/fem_config.hpp.in
    )
	
IF(Kokkos_FOUND)
    list(APPEND FEM_HEADERS
	    )
ENDIF()

list(APPEND FEM_SOURCES
	base/fem_test.cpp
	)

IF(WITH_MPI)
    list(APPEND FEM_HEADERS
	        examples/mars_advection.hpp
	        examples/mars_poisson.hpp
	)
ENDIF()
IF(WITH_MPI)
    list(APPEND FEM_SOURCES
	)
ENDIF()


set(CMAKE_CXX_DEBUG "-g")

add_library(fem STATIC ${FEM_HEADERS} ${FEM_SOURCES})
target_link_libraries(fem
  #$<$<CONFIG:RelWithDebInfo>:asan>
 # $<$<CONFIG:RelWithDebInfo>:ubsan>
)
target_compile_options(fem PUBLIC
  -Wall -Wextra
  #$<$<CONFIG:RelWithDebInfo>:-fsanitize=address -fsanitize=leak -fsanitize=undefined -fsanitize-address-use-after-scope>
#  $<$<CONFIG:RelWithDebInfo>:-fsanitize=memory -fsanitize-memory-track-origins>
)
target_link_libraries(fem ${MPI_LIBRARIES})
target_link_libraries(fem ${MARS_LIBRARIES})
#target_compile_features(fem PUBLIC cxx_std_11)

IF(WITH_MPI)
    target_compile_definitions(fem PUBLIC FEM_HAVE_MPI)
ENDIF()

add_definitions("-std=c++11")
target_include_directories(fem PUBLIC base)
target_include_directories(fem PUBLIC examples)

IF(Kokkos_FOUND)
	MESSAGE(" Kokkos_INCLUDE_DIRS = ${Kokkos_INCLUDE_DIRS}")
	MESSAGE(" Kokkos_LIBRARIES = ${Kokkos_LIBRARIES}")
	MESSAGE(" Kokkos_TPL_LIBRARIES = ${Kokkos_TPL_LIBRARIES}")
	MESSAGE(" Kokkos_LIBRARY_DIRS = ${Kokkos_LIBRARY_DIRS}")
	
	IF(USE_CUDA AND NOT Kokkos_TPL_LIBRARIES MATCHES "cuda")
		MESSAGE(FATAL_ERROR "Enable Kokkos Cuda or unset USE_CUDA to continue with OpenMP!")
	ENDIF()
	
	target_include_directories(fem PUBLIC ${Kokkos_TPL_INCLUDE_DIRS} ${Kokkos_INCLUDE_DIRS})

	message(STATUS "Kokkos_INCLUDE_DIRS=${Kokkos_INCLUDE_DIRS}, ${Kokkos_TPL_INCLUDE_DIRS}")
	
	IF(Kokkos_CXX_COMPILER)
		target_link_libraries(fem ${Kokkos_LIBRARIES} ${Kokkos_TPL_LIBRARIES})
	ELSE()
		target_link_libraries(fem ${Kokkos_LIBRARIES} ${Kokkos_TPL_LIBRARIES} -L${Kokkos_LIBRARY_DIRS})
	ENDIF()
ENDIF()

if(MARS_FOUND)
    MESSAGE(" MARS_INCLUDE_DIRS = ${MARS_INCLUDES}")
    MESSAGE(" MARS_LIBRARIES = ${MARS_LIBRARIES}")
    target_include_directories(fem PUBLIC ${MARS_INCLUDES})
    target_link_libraries(fem ${MARS_LIBRARIES})
endif()

add_executable(fem_exec main.cpp)
target_link_libraries(fem_exec fem)

if(TRY_WITH_MOONOLITH)
	add_subdirectory(moonolith_adapter)
endif()

target_link_libraries(fem ${FEM_LIBRARIES})
target_include_directories(fem PUBLIC ${CMAKE_BINARY_DIR})
target_include_directories(fem PUBLIC ${FEM_INCLUDES})

message(STATUS "${FEM_INCLUDES}")

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/base/fem_config.hpp.in ${CMAKE_BINARY_DIR}/fem_config.hpp)
include_directories(${CMAKE_BINARY_DIR})

install(TARGETS fem fem_exec
	RUNTIME DESTINATION bin 
	LIBRARY DESTINATION lib 
	ARCHIVE DESTINATION lib 
	)

install(FILES ${FEM_HEADERS} DESTINATION include)
install(FILES ${CMAKE_BINARY_DIR}/fem_config.hpp DESTINATION include)


# add_definitions("-DWITH_MOONOLITH")
# set(MOONOLITH_DIR "$ENV{INSTALL_DIR}/moon")
# target_include_directories(fem_exec PUBLIC ${MOONOLITH_DIR}/include)
# target_link_libraries(fem_exec
# 	-L${MOONOLITH_DIR}/lib/
# 	-leps
# 	-lexternal
# 	-lintersection
# 	-lio
# 	-lmeshing
# 	-lmoonolith
# 	-lquadrature
# 	-lshape
# 	-lutility
# )
